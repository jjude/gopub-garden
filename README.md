# Publish Your Digital Garden

`gopub` is a tool to publish your digital garden. If you don't know what a digital garden is, think of it as a folder of markdown notes that you have collected and created.

## See gopub in action

![gopub in action](/docs/assets/gopub.jpg)

You can read my published notes at [https://notes.jjude.com](https://notes.jjude.com).

## Features

- convert .md files into htmls ✅
- convert filenames into slugs ✅
- support markdown links ✅
- support [[wikilinks]] ✅
- support images of format ![[image]] ✅
- simple design based on tachyons ✅
- indicate external links differently ✅
- open external links in different tab  ✅
- sitewide search
- read metadata for titles, if present
- support tags in the format of #tags
- backlinks

## 🚀 Quick Start

### For publishing via gitlab

You need a [gitlab](https://gitlab.com/) account, if you already don't have it.

1. Fork this repository
2. Download the repository
3. Change the `index.md` file
4. Add more files to the `docs` directory
5. Modify the `site_name`, `site_author`, and `site_domain` in `gopub.yml`
6. Checkin the files to repository

### For publishing via github

- coming up

### For publishing elsewhere

If it is a linux based system, this should work well. If the OS is different then you have to build from the [source](https://gitlab.com/jjude/gopub).


## Licence

You are free to use it anyway you want.

## Source Code

You can look at the source code at [gitlab](https://gitlab.com/jjude/gopub). If you are interested, you can compile it and run for other platforms.

## Inspirations for your Digital Garden

- [My Hypertext Notes](https://notes.jjude.com/)
- [Blue Book](https://lyz-code.github.io/blue-book/)
- [Mental Nodes](https://www.mentalnodes.com/)

## Other ways for publishing your notes

- [Mindstone](https://mindstone.tuancao.me/)
- [Flowershow](https://flowershow.app/)
- [Obsidian Digital Garden](https://github.com/oleeskild/obsidian-digital-garden)
- [Gatsby Garden](https://github.com/binnyva/gatsby-garden/)
- [MkDocs](https://lyz-code.github.io/blue-book/writing/build_your_own_wiki/)
- [Emanote](https://emanote.srid.ca/)
- [Quartz](https://github.com/jackyzha0/quartz)
- [Digital Garden Using Jekyll](https://github.com/maximevaillancourt/digital-garden-jekyll-template)